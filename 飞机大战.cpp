#include<graphics.h>
#include<conio.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>


#pragma comment(lib,"Winmm.lib")

#define high 600
#define width 590

//全局变量
IMAGE img_bk,planeNormal1,planeNormal2;//背景，我方飞机
IMAGE img_bullet1,img_bullet2;//子弹
IMAGE img_enemyplane1,img_enemyplane2;
IMAGE img_planexplode1,img_planexplode2;
int enemy_x,enemy_y;//敌机坐标
int bullet_x,bullet_y;//子弹坐标
int plane_x,plane_y;//飞机坐标
int score;//得分


void starup()//数据初始化
{
	initgraph(width,high);

	score=0;

	plane_x=width*0.5;
	plane_y=high*0.9;

	bullet_x=plane_x;
	bullet_y=-120;

	enemy_x=width*0.5;
	enemy_y=0;

	mciSendString("open D:\\飞机大战\\game_music.mp3 alias bkmusic", NULL, 0, NULL);//打开背景音乐
	mciSendString("play bkmusic repeat", NULL, 0, NULL);  // 循环播放

	loadimage(&img_bk,_T("D:\\飞机大战\\background.jpg"));

	loadimage(&planeNormal1,_T("D:\\飞机大战\\planeNormal_1.jpg"));
	loadimage(&planeNormal2,_T("D:\\飞机大战\\planeNormal_2.jpg"));

	loadimage(&img_bullet1,_T("D:\\飞机大战\\bullet1.jpg"));
	loadimage(&img_bullet2,_T("D:\\飞机大战\\bullet2.jpg"));

	loadimage(&img_enemyplane1,_T("D:\\飞机大战\\enemyPlane1.jpg"));
	loadimage(&img_enemyplane2,_T("D:\\飞机大战\\enemyPlane2.jpg"));

	loadimage(&img_planexplode1,_T("D:\\飞机大战\\planeExplode_1.jpg"));
	loadimage(&img_planexplode2,_T("D:\\飞机大战\\planeExplode_2.jpg"));


	BeginBatchDraw();
} 

void show()//画面显示
{
	putimage(0,0,&img_bk);//背景

	putimage(plane_x-60,plane_y-60,&planeNormal1,NOTSRCERASE);
	putimage(plane_x-60,plane_y-60,&planeNormal2,SRCINVERT);

	putimage(bullet_x-14,bullet_y,&img_bullet1,NOTSRCERASE);
	putimage(bullet_x-14,bullet_y,&img_bullet2,SRCINVERT);

	putimage(enemy_x-52,enemy_y,&img_enemyplane1,NOTSRCERASE);
	putimage(enemy_x-52,enemy_y,&img_enemyplane2,SRCINVERT);

	
	outtextxy(width*0.45, high*0.9, "得分：");
	char s[5];
	sprintf(s, "%d", score);
	outtextxy(width*0.45, high*0.95, s);

	FlushBatchDraw();
	Sleep(50);
} 
 
 void updatewithoutinput()//与用户输入无关的更新
 {
	bullet_y-=30;
	enemy_y+=5;
	if(enemy_y>=high)
	{
		enemy_x=rand()%width;
	    enemy_y=0;
	}

	if(abs(enemy_x-bullet_x)+abs(enemy_y-bullet_y)<50)
	{
		enemy_x=rand()%width;
	    enemy_y=0;
		score++;
		mciSendString("close gemusic", NULL, 0, NULL); // 先把前面一次的音乐关闭
		mciSendString("open D:\\飞机大战\\gotEnemy.mp3 alias gemusic", NULL, 0, NULL); // 打开音乐
		mciSendString("play gemusic", NULL, 0, NULL); // 仅播放一次
		if ( score%5==0 && score%2!=0)
		{
			mciSendString("close 5music", NULL, 0, NULL); // 先把前面一次的音乐关闭
			mciSendString("open D:\\飞机大战\\5.mp3 alias 5music", NULL, 0, NULL); // 打开音乐
			mciSendString("play 5music", NULL, 0, NULL); // 仅播放一次
		}
		if (score%10==0)
		{
			mciSendString("close 10music", NULL, 0, NULL); // 先把前面一次的音乐关闭
			mciSendString("open D:\\飞机大战\\10.mp3 alias 10music", NULL, 0, NULL); // 打开音乐
			mciSendString("play 10music", NULL, 0, NULL); // 仅播放一次
		}
	}
 }
 
void updatewithinput()//与用户输入有关的更新 
{
	MOUSEMSG m;	// 定义鼠标消息	
	while(MouseHit())
	{
		// 获取一条鼠标消息
		m = GetMouseMsg();	
		if(m.uMsg == WM_MOUSEMOVE)//鼠标移动时，飞机坐标等于鼠标坐标
		{
			plane_x=m.x;
			plane_y=m.y;
		}
		else if(m.uMsg == WM_LBUTTONDOWN)//按鼠标左键发射子弹
		{
			bullet_x=plane_x;
	        bullet_y=plane_y-120;
		}
	}
}

void gameover()
{
	EndBatchDraw();
	getch();
	closegraph();
}

 int main()
 {
 	starup();//数据初始化
	 while(1)//游戏循环执行
	 {
	 	show();//画面显示
		 updatewithoutinput();//与用户输入无关的更新
		 updatewithinput();//与用户输入有关的更新 
	  } 
	 gameover();
 }
